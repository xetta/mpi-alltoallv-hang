PATH=$INSTALLDIR/bin:$PATH
PYTHONPATH=$INSTALLDIR/lib/python2.7/site-packages:$PYTHONPATH
LD_LIBRARY_PATH=$INSTALLDIR/lib:$LD_LIBRARY_PATH

# Number of processes to use during build
: ${PROCS:=8}

# Extract any extra argument
CMAKE_EXTRA_ARGS=$@

# Set installation and build directory
BUILDDIR=../dolfin/build
echo "Build and install in $BUILDDIR, $INSTALLDIR."
echo

#clean
cd ../dolfin; git clean -dfx

# Configure
mkdir -p $BUILDDIR
cd $BUILDDIR

cmake -DCMAKE_INSTALL_PREFIX=$INSTALLDIR \
     -DDOLFIN_SKIP_BUILD_TESTS=ON \
     -DDOLFIN_AUTO_DETECT_MPI=false \
     -DDOLFIN_ENABLE_UNIT_TESTS=false \
     -DDOLFIN_ENABLE_BENCHMARKS=false \
     -DDOLFIN_DEPRECATION_ERROR=true \
     -DDOLFIN_ENABLE_PYTHON=false \
     -DCMAKE_PREFIX_PATH=$INSTALLDIR \
     -DEIGEN3_INCLUDE_DIR=/usr/common/usg/eigen3/3.2.0/include/eigen3 \
     -DSLEPC_DIR=$INSTALLDIR \
     -DCMAKE_BUILD_TYPE=Debug \
     -DDOLFIN_ENABLE_QT=false \
     -DDOLFIN_ENABLE_VTK=false \
     -DDOLFIN_ENABLE_SLEPC=false \
     -DCMAKE_EXE_LINKER_FLAGS=$FLAGS \
     $CMAKE_EXTRA_ARGS \
     ..

# Build
make -j$PROCS && make install -j$PROCS
