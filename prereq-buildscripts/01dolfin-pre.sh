PYTHONPATH=$INSTALLDIR/lib/python2.7/site-packages:$PYTHONPATH
set -x
set -e

mkdir -p $INSTALLDIR/lib/python2.7/site-packages

pylist="sympy ply fiat instant ufl ffc"
cd ..

for i in $pylist
do
  cd $i
  git clean -fdx
  python setup.py install --prefix=${INSTALLDIR}
  cd ..
done

