cd ../petsc
git clean -dfx
petsc_pwd=`pwd`
FLAGS="-dynamic"
LINKFLAGS="-dynamic -O3 -Wl,-Bdynamic"
./configure \
--prefix=$INSTALLDIR \
--with-make-np=8 \
--with-c-support \
--with-clanguage=C++ \
--with-debugging=0 \
--with-shared-libraries=1 \
--CC=cc \
--CXX=CC \
--FC=ftn \
--CC_LINKER_FLAGS=$LINKFLAGS \
--CXX_LINKER_FLAGS=$LINKFLAGS \
--FC_LINKER_FLAGS=$LINKFLAGS \
--CFLAGS=$FLAGS \
--CXXFLAGS=$FLAGS \
--FFLAGS=$FLAGS \
--with-mpi-dir=$MPICH_DIR \
--with-blas-lib=[$CRAY_LIBSCI_PREFIX_DIR/lib/libsci_gnu.so] \
--with-lapack-lib=[$CRAY_LIBSCI_PREFIX_DIR/lib/libsci_gnu.so] \
--with-scalapack-lib=[$CRAY_LIBSCI_PREFIX_DIR/lib/libsci_gnu.so] \
--with-scalapack-include=$CRAY_LIBSCI_PREFIX_DIR/include \
--with-scalapack=1 \
--with-blacs-lib=[$CRAY_LIBSCI_PREFIX_DIR/lib/libsci_gnu.so] \
--with-blacs-include=$CRAY_LIBSCI_PREFIX_DIR/include \
--with-blacs=1\
--with-fortran-interfaces=1 \
--with-ptscotch=1 \
--download-ptscotch=1 \
--with-hypre=1 \
--download-hypre=1 \
--with-superlu_dist=1 \
--download-superlu_dist=1 \
--with-suitesparse=1 \
--download-suitesparse=1 \
--with-metis=1 \
--download-metis=1 \
--with-ml=1 \
--download-ml=1 \
--with-parmetis=1 \
--download-parmetis=1 \
--with-spai=1 \
--download-spai=1
make PETSC_DIR=$petsc_pwd PETSC_ARCH=arch-linux2-cxx-opt all
make PETSC_DIR=$petsc_pwd PETSC_ARCH=arch-linux2-cxx-opt install
