Make sure you have the follow modules loaded in your environment before attempting to run cmake.

boost/1.55
cmake/3.0.0
PrgEnv-gnu/5.2.56

Set the env variable $INSTALLDIR to the directory you unpack this into. Make sure that the following vars include $INSTALLDIR (I set this in my .bashrc.ext):

export INSTALLDIR=<wherever I unpack tarball>
export PATH=$INSTALLDIR/bin:$PATH
export PYTHONPATH=$INSTALLDIR/lib/python2.7/site-packages:$PYTHONPATH
export LD_LIBRARY_PATH=$INSTALLDIR/lib:$LD_LIBRARY_PATH

in this dir, it's enough to run:

gunzip mesh_783k.h5.gz 
'cmake .'
make 

Then run qsub mpialltoalv-test.pbs with mppwidth=1056 and 'aprun -n 1056' to see issue. It's fine if running with smaller number of procs 
