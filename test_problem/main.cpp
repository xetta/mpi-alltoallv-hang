#include <dolfin.h>

using namespace dolfin;

int main(int argc, char *argv[])
{
 //  parameters["mesh_partitioner"]="ParMETIS";

 //fprintf(stderr,"In main at top\n");

 Mesh mesh;
 HDF5File h5file(mesh.mpi_comm(),
                 "mesh_783k.h5", "r");

 //fprintf(stderr,"In main before read\n");

 h5file.read(mesh, "/mesh", false);

//fprintf(stderr,"In main after read\n");

 const std::size_t nv = mesh.size_global(0);
 const std::size_t mpi_size = dolfin::MPI::size(mesh.mpi_comm());
 if (dolfin::MPI::rank(mesh.mpi_comm()) == 0)
   {
     std::cout << "nv = " << nv << "\n";
     std::cout << "mpi_size = " << mpi_size << "\n";
   }

 list_timings(TimingClear::clear, std::set<TimingType>{TimingType::wall});

 return 0;
}
